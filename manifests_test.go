package main

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetListOfGlobMatches(t *testing.T) {
	//assert getManifestMatches gets all requirements.txt
	expected := []string{
		"subdir/requirements.txt",
		"a/b/c/d/e/f/g/requirements.txt",
	}
	got := getListOfGlobMatches("test/good_config", "**/requirements.txt")

	sort.Strings(expected)
	sort.Strings(got)

	assert.Equal(t, expected, got, "they should be equal")
}

func TestGetListOfManifestFilenames(t *testing.T) {
	expected := []string{
		"yarn.lock",
		"Gemfile",
		"a/b/c/d/e/f/g/requirements.txt",
		"subdir/requirements.txt",
	}
	got := getListOfManifestFilenames("test/good_config")

	sort.Strings(expected)
	sort.Strings(got)

	assert.Equal(t, expected, got, "they should match")
}
