package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli"
)

/*

	scan,s [directory-with-.tidelift.yml]
	configure,c [directory-with-.tidelift.yml]
	verify [directory-with-.tidelift.yml]
*/
func main() {
	app := cli.NewApp()

	app.Commands = []cli.Command{
		{
			Name:    "scan",
			Aliases: []string{"s"},
			Usage:   "Scan a directory's manifests",
			Action: func(c *cli.Context) error {
				everyCommand()
				directory := getDirectory(c)

				return scan(directory)
			},
		},
		{
			Name:  "verify",
			Usage: "Verify .tidelift.yml configuration and your ability to upload scans",
			Action: func(c *cli.Context) error {
				everyCommand()
				directory := getDirectory(c)

				return verify(directory)
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func getDirectory(c *cli.Context) string {
	// Return a directory if it's in the arguments
	if len(c.Args()) > 0 {
		path := c.Args().First()
		path = strings.TrimRight(path, "/")
		return path
	}
	return "."
}

/// everyCommand is things to run for every command
func everyCommand() error {
	if os.Getenv("TIDELIFT_API_KEY") == "" {
		return cli.NewExitError("please set TIDELIFT_API_KEY environment variable", 8)
	}
	return nil
}

// Scan will scan a directory's manifests for all supported manifest files
//  then it will upload them to tidelift for scanning
func scan(directory string) error {

	// Show error if no .tidelift.yml file
	if !verifyTideliftYamlExists(directory) {
		return cli.NewExitError("no .tidelift.yml at supplied directory path", 6)
	}
	fmt.Println(strings.Join(getListOfManifestFilenames(directory), ", "))
	upload(directory)
	// TODO, actually upload scan, don't just print names
	return nil
}

// Verify will verify that a .tidelift.yml file has the minimum information
// in order to upload a manifest to Tidelift
func verify(directory string) error {
	// Show error if no .tidelift.yml file

	if !verifyTideliftYamlExists(directory) {
		return cli.NewExitError("no .tidelift.yml at supplied directory path", 6)
	}

	missingKey := passesMinimumRequirements(readTideliftYamlFile(directory))
	if missingKey != "" {
		errorMsg := fmt.Sprintf("Missing key '%s:' in .tidelift.yml", missingKey)
		return cli.NewExitError(errorMsg, 7)
	}

	fmt.Println("Your team-name, repo-name and TIDELIFT_API_KEY are all set! Feel free to use `scan`")
	return nil
}

// Check error and panic if so
func check(e error) {
	if e != nil {
		panic(e)
	}
}
