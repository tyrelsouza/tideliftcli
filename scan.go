package main

import (
	"fmt"
	"os"

	"github.com/ddliu/go-httpclient"
)

func upload(directory string) {
	tideliftYML := getKeysFromYaml(readTideliftYamlFile(directory))
	fileNames := getListOfManifestFilenames(directory)
	auth := fmt.Sprintf("Bearer %s", os.Getenv("TIDELIFT_API_KEY"))
	// url := fmt.Sprintf("https://api.tidelift.com/subscriber/%s/%s/manifest/upload", team, repository)
	url := fmt.Sprintf("http://127.0.0.1:8000/subscriber/%s/%s/manifest/upload", string(tideliftYML.TeamName), string(tideliftYML.RepositoryName))
	// TODO: Add revision

	var body = map[string][]string{
		// maybe use httpclient.addFormFile and do everything manually...
		// https://github.com/ddliu/go-httpclient/blob/master/httpclient.go#L676
		"@files[]": fileNames,
	}

	cwd, err := os.Getwd()
	check(err)
	os.Chdir(directory)

	_, err = httpclient.WithHeaders(map[string]string{
		"Authorization": auth,
	}).Post(url, body)
	check(err)

	os.Chdir(cwd)

	fmt.Println(body)
}

func openFile(f string) *os.File {
	handle, err := os.Open(f)
	check(err)
	return handle
}
