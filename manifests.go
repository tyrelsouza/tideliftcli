package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/bmatcuk/doublestar"
)

var (
	/*
		When adding new manifest types, for new package managers
		 add a new entry here. If it's a new platform/package manager, please
		 update the comments with the platform name for organization sense
	*/
	manifestGlobs = []string{
		// Hackage
		"*.cabal",
		// npm
		"package.json",
		"package-lock.json",
		"npm-shrinkwrap.json",
		"yarn.lock",
		// Maven
		"pom.xml",
		"ivy.xml",
		"build.gradle",
		// RubyGems
		"Gemfile",
		"Gemfile.lock",
		"gems.rb",
		"gems.locked",
		"*.gemspec",
		// Packagist
		"composer.json",
		"composer.lock",
		// PyPi
		"setup.py",
		"req*.txt",
		"req*.pip",
		"requirements/*.txt",
		"requirements/*.pip",
		"Pipfile",
		"Pipfile.lock",
		// Nuget
		"packages.config",
		"Project.json",
		"Project.lock.json",
		"*.nuspec",
		"paket.lock",
		"*.csproj",
		// Bower
		"bower.json",
		// CPAN
		"META.json",
		"META.yml",
		// CocoaPods
		"Podfile",
		"Podfile.lock",
		"*.podspec",
		// Clojars
		"project.clj",
		// Meteor
		"versions.json",
		// CRAN
		"DESCRIPTION",
		// Cargo
		"Cargo.toml",
		"Cargo.lock",
		// Hex
		"mix.exs",
		"mix.lock",
		// Swift
		"Package.swift",
		// Pub
		"pubspec.yaml",
		"pubspec.lock",
		// Carthage
		"Cartfile",
		"Cartfile.private",
		"Cartfile.resolved",
		// Dub
		"dub.json",
		"dub.sdl",
		// Julia
		"REQUIRE",
		// Shards
		"shard.yml",
		"shard.lock",
		// Go
		"glide.yaml",
		"glide.lock",
		"Godeps",
		"Godeps/Godeps.json",
		"vendor/manifest",
		"vendor/vendor.json",
		"Gopkg.toml",
		"Gopkg.lock",
		// Elm
		"elm-package.json",
		"elm_dependencies.json",
		"elm-stuff/exact-dependencies.json",
		// Haxelib
		"haxelib.json",
		// Hackage
		"*.cabal",
		"cabal.config",
	}
)

// getListOfManifestFilenames: Given a directory, changes directory to that directory,
//  then loops through all of the support manifest file patterns, and
//  gets all matching instances
//
// returns string[] of filenames relative to the directory passed in.
func getListOfManifestFilenames(directory string) []string {
	matchingFiles := make([]string, 0)

	for _, glob := range manifestGlobs {
		files := getListOfGlobMatches(directory, glob)

		if len(files) > 0 {
			matchingFiles = append(matchingFiles, files...)
		}
	}
	return matchingFiles
}

// getListOfGlobMatches uses doublestar's Glob function to expand and find
//  all instances of a manifest file pattern, in your given directory
//
// returns string[] of all matching file patterns
func getListOfGlobMatches(directory string, glob string) []string {
	pathGlob := fmt.Sprintf("%s/**/%s", directory, glob)

	files, err := doublestar.Glob(pathGlob)
	if err != nil {
		log.Fatal(err)
	}

	files = stripDirectory(directory, files)
	return files
}

func stripDirectory(directory string, paths []string) []string {
	var cleaned []string

	for _, path := range paths {
		cleaned = append(cleaned, strings.TrimPrefix(path, directory+"/"))
	}
	return cleaned
}
