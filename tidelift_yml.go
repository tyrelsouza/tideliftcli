package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

type TideliftYML struct {
	TeamName       string `yaml:"team-name"`
	RepositoryName string `yaml:"repository-name"`
}

func verifyTideliftYamlExists(directory string) bool {
	filepath := fmt.Sprintf("%s/.tidelift.yml", directory)

	if fileExists(filepath) {
		return true
	}

	return false
}

func readTideliftYamlFile(directory string) []byte {
	filename, _ := filepath.Abs(fmt.Sprintf("%s/.tidelift.yml", directory))

	yamlFile, err := ioutil.ReadFile(filename)

	check(err)

	return yamlFile
}

func getKeysFromYaml(yamlText []byte) TideliftYML {
	var yml TideliftYML

	err := yaml.Unmarshal(yamlText, &yml)

	check(err)

	return yml
}

func passesMinimumRequirements(yamlText []byte) string {
	yml := getKeysFromYaml(yamlText)

	// Check for Team name
	if len(yml.TeamName) <= 0 {
		return "team-name"
	}

	// Check for Repository Name
	if len(yml.RepositoryName) <= 0 {
		return "repository-name"
	}

	return ""
}

func fileExists(filename string) bool {

	info, err := os.Stat(filename)

	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
