package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVerifyTideliftYamlExists(t *testing.T) {
	verifiedFalse := verifyTideliftYamlExists("test/no_config")
	assert.False(t, verifiedFalse, "should not be a .tidelift.yml in test/no_config")

	verifiedTrue := verifyTideliftYamlExists("test/bad_config")
	assert.True(t, verifiedTrue, "should be a .tidelift.yml in test/bad_config")
}

func TestPassesMinimumRequirements(t *testing.T) {
	missingTeam := []byte("")
	assert.Equal(t, "team-name", passesMinimumRequirements(missingTeam), "should be `team-name`")

	missingRepo := []byte("team-name: teamname")
	assert.Equal(t, "repository-name", passesMinimumRequirements(missingRepo), "should be `repository-name`")

	ok := []byte("team-name: teamname\nrepository-name: repo")
	assert.Equal(t, "", passesMinimumRequirements(ok), "should be ``")
}

func TestReadTideliftYamlFile(t *testing.T) {
	yamlFile := readTideliftYamlFile("test/good_config")
	assert.Equal(t,
		[]byte("team-name: tidelift\nrepository-name: fake\n"),
		yamlFile,
		"should have loaded file into []byte")
}
