# Install
Using go >= 1.12 so we have go modules.

```
$ go get
$ go build
$ go test
$ ./tideliftcli scan ~/test/good_fixtures
  yarn.lock, Gemfile, subdir/requirements.txt, a/b/c/d/e/f/g/requirements.txt
```



Deps

https://github.com/urfave/cli
https://github.com/bmatcuk/doublestar
https://github.com/stretchr/testify
