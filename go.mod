module gitlab.com/tyrelsouza/tideliftcli

go 1.12

require (
	github.com/bmatcuk/doublestar v1.1.1
	github.com/davecgh/go-spew v1.1.1
	github.com/ddliu/go-httpclient v0.5.1
	github.com/fatih/color v1.7.0 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/pmezard/go-difflib v1.0.0
	github.com/rakyll/gotest v0.0.0-20180125184505-86f0749cd8cc // indirect
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.20.0
	gopkg.in/yaml.v2 v2.2.2
)
